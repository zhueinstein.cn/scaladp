val bubbleSorter = {
  src: Array[Int] =>
  // fake bubble sort
    src.clone()
}
val quickSorter = {
  src: Array[Int] =>
  // fake quick sort
    src.clone()
}
val heapSorter = {
  src: Array[Int] =>
  // fake heap sort
    src.clone()
}

val data = Array.fill(100)((math.random * 100).toInt)

Array(bubbleSorter, quickSorter, heapSorter).foreach {
  sorter =>
    new Thread(new Runnable {
      def run() {
        sorter.apply(data.clone())
      }
    }).start()
}
