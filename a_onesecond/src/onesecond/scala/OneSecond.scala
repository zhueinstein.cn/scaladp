package onesecond.scala

import collection.immutable._

class OneSecond(elements: Int*) {
  def this(str: String) = this(str.toInt)

  def first = ints.head

  val ints:Vector[Int] = Vector(elements:_*)
  var num = 0

  println("Constructed.")
}

