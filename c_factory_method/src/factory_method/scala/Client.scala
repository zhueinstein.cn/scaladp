package factory_method.scala

object Client extends App {
	trait IFruit {
	  def taste: String
	}
	class Apple(val mature: Boolean) extends IFruit {
	  override def taste = if (mature) "good" else "bad"
	}
	object Apple {
	  def apply(mature: Boolean) = new Apple(mature)
	}
	
	class Orange(val mature: Boolean) extends IFruit {
	  override def taste = if (mature) "bad" else "good"
	}
	object Orange {
	  def apply(mature: Boolean) = new Orange(mature)
	}
	
	println(Apple(mature = true).taste)
	println(Apple(mature = false).taste)
	println(Orange(mature = true).taste)
	println(Orange(mature = false).taste)
}