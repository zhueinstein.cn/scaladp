package factory_method.java;

public class OrangeFactory implements IFruitFactory {

	@Override
	public IFruit createFruit(boolean mature) {
		return new Orange(mature);
	}
}
